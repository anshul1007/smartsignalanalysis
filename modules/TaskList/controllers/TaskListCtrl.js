//create the module and name it WarpIOTApp
(function() {
	'use strict';
	angular.module('WARPIOTAPP')
			.controller(
					"TasksController",
					function($scope, $http, $rootScope, NgTableParams) {
						$scope.Reverse = false;
						$scope.streetAddress1;
						$scope.city;
						$scope.postalCode;
						$scope.store;
						$scope.state;
						$scope.storePhone;
						$scope.storeManager;
						$scope.SortKey;
						$scope.Tasks = [];
						$scope.SelectedTasks = [];
						$scope.SelectedTaskID;
						$scope.SelectedEventID;
						$scope.Active;
						$scope.showSection = false;
						$scope.WOrders = [];
						$scope.SEvents = [];
						$scope.QEvents = [];
						$scope.SetTaskDetails = function(cc,store,lang,rid,mid,iotype,sensid) 
							{
								var varArr=[cc,store,lang,rid,mid,iotype,sensid];
								$rootScope.tListTrendNav= varArr.join("/");
							};
						$scope.getHeader = function() {
							return [ "Task ID", "Priority", "Status", "User ID",
									"Store", "State", "Sensor Name", "Timestamp",
									"WorkOrder" ]};
						$scope.getColumns = function() {
							return [ "taskId", "priority", "status", "assignedTo",
									"store", "state", "sensorName", "timestamp",
									"workOrder" ];};
						// Define function for loading the Tasks Table.
						$scope.LoadTable = function() {
							var httpRequest = $http.get(
									'http://172.30.86.252:8080/getTaskList?countryCode=US&language=101')
									.success(function(data, status) {
										$scope.Tasks = null;
										$scope.Tasks = data;
										angular.forEach($scope.Tasks, function(task) {
									var baseUrl = "https://api.qa.wal-mart.com/si/telocmdm/locationservices/location/US/100/businessunits/";
									var httpUrl = baseUrl + task.store;
									var httpRequest = $http.get(
											httpUrl, {
												params : {
													 q:"details",
													 client_id:"6fda021d-e9f5-4702-9285-51929d433c6d"
												}
											}).success(
											function(data, status) {
												$scope.StoreDetails = null;
												$scope.StoreDetails = data;
												task.state=$scope.StoreDetails.businessUnitDetail[0].businessUnit.address[1].state;
												task.streetAddress1= $scope.StoreDetails.businessUnitDetail[0].businessUnit.address[1].addressLine1;
												task.city=$scope.StoreDetails.businessUnitDetail[0].businessUnit.address[1].city;
												task.postalCode=$scope.StoreDetails.businessUnitDetail[0].businessUnit.address[1].postalCode;
												task.storePhone=$scope.StoreDetails.businessUnitDetail[0].businessUnit.contact[0].phone[0].number;
												task.storeManager=$scope.StoreDetails.businessUnitDetail[0].businessUnit.manager.name.fullName;
											});
								});
										$scope.tableParams = new NgTableParams({
											counts : []
										}, {
										dataset : $scope.Tasks
										});
									});
						};
				// Call the tasks table function for loading the Tasks Table.
				$scope.LoadTable();
				$scope.HighPriorityCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.priority == 1)
							count = count + 1;
					});
					return count;
				};
				$scope.MediumPriorityCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.priority == 2)
							count = count + 1;
					});
					return count;
				};
				$scope.LowPriorityCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.priority == 3)
							count = count + 1;
					});
					return count;
				};
				$scope.EmerPriorityCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.priority == 0)
							count = count + 1;
					});
					return count;
				};
				$scope.OpenTaskCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.status == "OPEN")
							count = count + 1;
					});
					return count;
				};
				$scope.WIPTaskCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.status == "WIP")
							count = count + 1;
					});
					return count;
				};
				$scope.ClosedTaskCount = function() {
					var count = 0;
					angular.forEach($scope.Tasks, function(task) {
						if (task.status == "CLOSED")
							count = count + 1;
					});
					return count;
				};
				$scope.getTableForExport = function() {
					if ($scope.SelectedTasks.length == 0)
						return $scope.Tasks;
					else
						return $scope.SelectedTasks;
				};
				var returnIndex = function(taskId) {
					for (var i = 0; i < $scope.SelectedTasks.length; i++) {
						if ($scope.SelectedTasks[i].taskId === taskId) {
							return i;
						}
					}
					return -1;
				}
				var updateSelected = function(action, task) {
					if (action == 'add'
							&& returnIndex(task.taskId) == -1)
						$scope.SelectedTasks.push(task);
					if (action == 'remove'
							&& returnIndex(task.taskId) != -1)
						$scope.SelectedTasks.splice(returnIndex(task.taskId), 1);
				};
				$scope.updateSelection = function($event, task) {
					var checkbox = $event.target;
					var action = (checkbox.checked ? 'add' : 'remove');
					updateSelected(action, task);
				};
				$scope.isSelected = function(id) {
					return $scope.SelectedTasks.indexOf(id) >= 0;
				};
						// Maps the Priority code to its equivalent text
						$scope.GetPriority = function(priorityCode) {
							switch (priorityCode) {
							case 0:
								return 'EMERGENCY';
							case 1:
								return 'CRITICAL';
							case 2:
								return 'MEDIUM';
							case 3:
								return 'LOW';
							}
						}
						// Define function for Work Orders table loading. Send TaskId as parameter.
						$scope.LoadWOTable = function() {
							var httpRequest = $http.get(
									'http://172.19.171.240:8080/getTableData',
									{
										params : {
											TaskID : $scope.SelectedTaskID
										}
									}).success(function(data, status) {
								$scope.WOrders = data;
							});
						};
						// Define function for Qualifying Events table loading. Send TaskId as parameter.
						$scope.LoadQETable = function() {
							var httpRequest = $http.get(
									'http://172.30.87.243:8080/getTableData', {
										params : {
											TaskID : $scope.SelectedTaskID
										}
									}).success(function(data, status) {
								$scope.QEvents = data;
							});
						};
						// Define function for Significant Events table loading. Send TaskId as parameter.
						$scope.LoadSETable = function() {
							var httpRequest = $http.get(
									'http://172.30.87.243:8080/getTableData', {
										params : {
											TaskID : $scope.SelectedTaskID
										}
									}).success(function(data, status) {
								$scope.SEvents = data;
							});
						};
						// Define function that sets the value of selectedRow in TaskList
						$scope.SetClickedRowValues = function(TaskID,streetAddress1,city,postalCode,store,state,storePhone,storeManager) {
							if ($scope.SelectedTaskID != TaskID) {
								$scope.SelectedTaskID = TaskID;
								$scope.streetAddress1 = streetAddress1;
								$scope.city = city;
								$scope.postalCode = postalCode;
								$scope.store = store;
								$scope.state = state;
								$scope.storePhone = storePhone;
								$scope.storeManager = storeManager;
						
							} else {
								$scope.SelectedTaskID = null;
								$scope.streetAddress1 = null;
								$scope.city = null;
								$scope.postalCode = null;
								$scope.store = null;
								$scope.state = null;
								$scope.storePhone = null;
								$scope.storeManager = null;
							}
							$scope.LoadWOTable();
							$scope.LoadQETable();
							$scope.LoadSETable();
						};
						$scope.SetClickedEventRowValues = function(EventID) {
							if ($scope.SelectedEventID != EventID) {
								$scope.SelectedEventID = EventID;
							} else {
								$scope.SelectedEventID = null;
							}
						};
					});
})();