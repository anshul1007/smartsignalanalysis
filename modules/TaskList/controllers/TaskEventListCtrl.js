(function() {
	'use strict';

	angular.module('WARPIPTAPP')

	.controller(
			"TasksWOController",
			function($scope, $http) {
				$scope.RelTasks = [];
				$scope.SelectedWOrder;
				// Define function for Related Tasks table loading. Send Work
				// Order Id as
				// parameter.
				$scope.LoadRelTasksTable = function() {
					var httpRequest = $http.get(
							'http://172.30.87.243:8080/getTableData', {
								params : {
									TaskID : $scope.SelectedWOrder
								}
							}).success(function(data, status) {
						$scope.RelTasks = data;
					});
				};

				// Define function that sets the value of selectedRow in Work
				// Order List
				$scope.SetClickedWORowValues = function(WOrderID) { // function
					// that sets
					// the value of
					// selectedRow
					$scope.SelectedWOrder = WOrderID;
					$scope.LoadRelTasksTable();
				};

			});
})();
