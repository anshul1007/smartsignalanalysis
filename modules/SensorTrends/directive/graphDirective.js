warpIOTApp.directive('rickshawChart', function($window) {
	return {
		scope : {
			data : '=',
			renderer : '=',
			type : '=',
			yhigh : "=",
			ylow : "="
		},
		template : '<div id="chart"></div><div class="text-center horizontaltext_content" style="margin-top: 30px;margin-bottom: 15px;">'
					+ 'Time Interval (in CST)&nbsp;<span class="glyphicon glyphicon-arrow-right"></span></div>'
					+ '<div id="slider" style="background-color: darkslateblue;"></div>',
		restrict : 'E',
		link : function postLink(scope, element, attrs) {
			// console.log(" Directive");
			var chart = "";
			scope.$watchCollection('[data, renderer,type,YHigh,YLow]', function(newVal, oldVal) {
				if (!newVal[0]) {
					return;
				}
				chart = document.getElementById('chart');
				chart.innerHTML = "";
				var graph = "";
				var yAxis = "";
				var format = function(n) {

					var map = {
						0 : scope.ylow,
						1 : scope.yhigh

					};

					return map[n];
				}
				//console.log("Scope " + scope.data.length);

				if (scope.type == "analog") {
					graph = new Rickshaw.Graph({
						element : document.getElementById('chart'),
						height : 300,
						series : scope.data,
						min : getMinValue(scope.data.length) - 5,
						max : 5 + getMaxValue(scope.data.length),
						renderer: getRenderer(scope.data.length),
						interpolation : "linear"
					});

					yAxis = new Rickshaw.Graph.Axis.Y({
						graph : graph
					});
				} else if (scope.type == "digital") {
					graph = new Rickshaw.Graph({
						element : document.getElementById('chart'),
						height : 300,
						max : 1.25,
						min : -0.25,
						series : scope.data,
						renderer : getRenderer(scope.data.length),
						interpolation : "linear"
					});

					yAxis = new Rickshaw.Graph.Axis.Y({
						graph : graph,
						tickFormat : format,
						tickValues : [ 0, 1 ]
					});
				}

				// var time = new
				// Rickshaw.Fixtures.Time();
				// var seconds =
				// time.unit('hour');

				var xAxis = new Rickshaw.Graph.Axis.Time({
					graph : graph,
					timeFixture : new Rickshaw.Fixtures.Time.Local()

				});

				var hoverDetails = new Rickshaw.Graph.HoverDetail({
					graph : graph,
					formatter : function(series, x, y, formattedX, formattedY) {
						if (scope.type == "digital") {
							if (y == 1) {
								y = scope.yhigh;
							}
							if (y == 0)
								y = scope.ylow;
						}
					//	console.log("Formatted X  " + formattedX);
						return " Reading " + y + "    Time   " + new Date(formattedX).toLocaleTimeString('en-US', {
							hour12 : false
						});
						;
					}
				})
				xAxis.render();
				yAxis.render();
				graph.render();

				var slider = new Rickshaw.Graph.RangeSlider({
					graph : graph,
					element : document.getElementById('slider')
				});

				function getMaxValue(pSeriesLength) {
					if (pSeriesLength == 1) {
						return Math.max.apply(Math, (scope.data[0].data).map(function(o) {
							return o.y;
						}));
					} else {
						var lmax = Math.max.apply(Math, (scope.data[0].data).map(function(o) {
							return o.y;
						}))
						var highThresh = Math.max.apply(Math, (scope.data[1].data).map(function(o) {
							return o.y;
						}))
						var lowThresh = Math.max.apply(Math, (scope.data[2].data).map(function(o) {
							return o.y;
						}))

						if (lmax > highThresh) {
							if (lmax > lowThresh) {
								return lmax;
							} else {
								return lowThresh;
							}
						} else {
							lmax = highThresh;
							if (lmax > lowThresh) {
								return lmax;
							} else {
								return lowThresh;
							}
						}
					}

				}

				function getMinValue(pSeriesLength) {
					if (pSeriesLength == 1) {
						return Math.min.apply(Math, (scope.data[0].data).map(function(o) {
							return o.y;
						}));
					} else {
						var lmax = Math.min.apply(Math, (scope.data[0].data).map(function(o) {
							return o.y;
						}))
						var highThresh = Math.min.apply(Math, (scope.data[1].data).map(function(o) {
							return o.y;
						}))
						var lowThresh = Math.min.apply(Math, (scope.data[2].data).map(function(o) {
							return o.y;
						}))

						if (lmax < highThresh) {
							if (lmax < lowThresh) {
								return lmax;
							} else {
								return lowThresh;
							}
						} else {
							lmax = highThresh;
							if (lmax < lowThresh) {
								return lmax;
							} else {
								return lowThresh;
							}
						}
					}

				}
				
				function getRenderer(pSeries){
					if(pSeries==1){
						return "lineplot"
					}else{
						return "multi"
					}
				}

			});
		}
	};
});
