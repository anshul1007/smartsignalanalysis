(function() {

	// test controller
	warpIOTApp.controller('SensorTreeCtrl', function($scope,$state ,$$SensorTrendSensorList, $interval, $$Constant, $$SensorTrendStoreServ, $rootScope, $window,
			$http, $$Helper, $$SharedDataService, $$SensorTrendDetailsServ) {
		// console.log('SensorTreeCtrl');
		$rootScope.loading = true;
		$scope.sensorQuery = '100/2/EP2/24';
		var mSensorSelectedData = "";
		var freezeInterval;

		function mapper(input, sensorQuery, isAll) {
			/* var start = new Date().getTime(); */
			var output = [];
			var sensorQueryArray = sensorQuery.split("/");
			var len = sensorQueryArray.length;
			$scope.treeSearchQuery = '';

			// var select = "COMP AS22";
			$.each(input, function(index, value) {

				var matchSearch = (!!$scope.treeSearchQuery);
				var foundNode = false;
				if (matchSearch && (value.countryCode.indexOf($scope.treeSearchQuery) > -1)
						|| (value.storeNbr && value.storeNbr.toString().indexOf($scope.treeSearchQuery) > -1)
						|| (value.rackIndex && value.rackIndex.toString().indexOf($scope.treeSearchQuery) > -1)
						|| (value.rackLabel && value.rackLabel.indexOf($scope.treeSearchQuery) > -1)
						|| (value.modIndex && value.modIndex.toString().indexOf($scope.treeSearchQuery) > -1)
						|| (value.modLabel && value.modLabel.indexOf($scope.treeSearchQuery) > -1)
						|| (value.sensorReadingType && value.sensorReadingType.indexOf($scope.treeSearchQuery) > -1)
						|| (value.sensorReadingType && value.sensorReadingType.indexOf($scope.treeSearchQuery) > -1)
						|| (value.sensorIndex && value.sensorIndex.toString().indexOf($scope.treeSearchQuery) > -1)
						|| (value.sensorlabel && value.sensorlabel.indexOf($scope.treeSearchQuery) > -1)) {
					foundNode = true;
				} else if (matchSearch) {
					return;
				}

				var countryCode = _.find(output, {
					"FieldName" : value.countryCode
				});
				if (!countryCode) {
					countryCode = {
						"FieldName" : value.countryCode,
						"expanded" : true,
						'level' : 1,
						"children" : []
					};
					output.push(countryCode);
				}

				var store = _.find(countryCode.children, {
					"FieldName" : 'Store ' + value.storeNbr
				})
				if (!store) {
					var store = {
						"FieldName" : 'Store ' + value.storeNbr,
						"expanded" : true,
						"countryCode" : value.countryCode,
						'level' : 2,
						"children" : []
					}
					countryCode.children.push(store);
				}

				var rack = _.find(store.children, {
					"FieldName" : value.rackIndex + "-" + value.rackLabel
				})
				if (!rack) {
					var rack = {
						"FieldName" : value.rackIndex + "-" + value.rackLabel,
						"expanded" : (typeof sensorQueryArray[1] !== 'undefined' && sensorQueryArray[1] !== ''),
						"countryCode" : value.countryCode,
						"storeNbr" : value.storeNbr,
						"rackIndex" : value.rackIndex,
						'level' : 3,
						"children" : []
					}
					store.children.push(rack);
				}

				if (isAll || (len > 1 && (typeof sensorQueryArray[1] !== 'undefined' && sensorQueryArray[1] !== ''))) {
					if (value.modIndex == '0') {
						var module = rack;
					} else {
						var module = _.find(rack.children, {
							"FieldName" : value.modIndex + "-" + value.modLabel
						})
						if (!module) {
							var module = {
								"FieldName" : value.modIndex + "-" + value.modLabel,
								"expanded" : (typeof sensorQueryArray[3] !== 'undefined' && sensorQueryArray[3] !== ''),
								"countryCode" : value.countryCode,
								"storeNbr" : value.storeNbr,
								"rackIndex" : value.rackIndex,
								"modIndex" : value.modIndex,
								'level' : 4,
								"children" : []
							}
							rack.children.push(module);
						}
					}
					if (isAll || (len > 3 && (typeof sensorQueryArray[3] !== 'undefined' && sensorQueryArray[3] !== ''))) {
						if (value.sensorReadingType == '') {
							var sensorType = module;
						} else {
							var sensorType = _.find(module.children, {
								"FieldName" : value.sensorReadingType + "-" + value.sensorIOType
							})
							if (!sensorType) {
								var sensorType = {
									"FieldName" : value.sensorReadingType + "-" + value.sensorIOType,
									"expanded" : (typeof sensorQueryArray[4] !== 'undefined' && sensorQueryArray[4] !== ''),
									'level' : 5,
									"children" : []
								}
								module.children.push(sensorType);
							}
						}
						var sensor_Label = _.find(sensorType.children, {
							"FieldName" : value.sensorIndex + "-" + value.sensorlabel
						})

						if (!sensor_Label) {
							var sensor_Label = {
								"FieldName" : value.sensorIndex + "-" + value.sensorlabel,
								"fileicon" : true,
								"value" : value,
								'level' : 6,
								"isSelected" : (value.sensorIndex && value.sensorIndex.toString() === sensorQueryArray[5])
							}
							sensorType.children.push(sensor_Label);
						}
					}
				}

			});
			/*
			 * var end = new Date().getTime(); console.log(start - end);
			 */
			return output;
		}
		$rootScope.findRackchildren = function(countryCode, storeNbr, rackIndex ){
			
			var rackChildren = [];
			$.each($rootScope.responseData, function(index, value) {
				if(value.countryCode === countryCode && value.storeNbr === storeNbr  && value.rackIndex === rackIndex){
					 	if(value.modIndex!==0){
					 		var module = _.find(rackChildren, {
			                "FieldName": value.modIndex + "-" + value.modLabel
			            })
			            if (!module) {
			                var module = {
			                    "FieldName": value.modIndex + "-" + value.modLabel,
			                    //"expanded": (typeof sensorQueryArray[3] !== 'undefined' && sensorQueryArray[3]!==''),
			                    "countryCode": value.countryCode,
				                "storeNbr": value.storeNbr,
				                "rackIndex": value.rackIndex,
				                "modIndex": value.modIndex,
			                    'level': 4,
			                    "children": []
			                }
			                rackChildren.push(module);
			            }
					 	}
			            else{
			            	var sensorType = _.find(rackChildren, {
			                "FieldName": value.sensorReadingType + "-" + value.sensorIOType
			            })
			            if (!sensorType) {
			                var sensorType = {
			                    "FieldName": value.sensorReadingType + "-" + value.sensorIOType,
			                    
			                    'level': 5,
			                    "children": []
			                }
			                rackChildren.push(sensorType);
			            }
				        var sensor_Label = _.find(sensorType.children, {
				            "FieldName": value.sensorIndex + "-" + value.sensorlabel
				        })

				        if (!sensor_Label) {
				            var sensor_Label = {
				                "FieldName": value.sensorIndex + "-" + value.sensorlabel,
				                "fileicon": true,
				                "value": value,
				                'level': 6,
				                
				            }
				            sensorType.children.push(sensor_Label);
				        }
				        }
					 	
				
				}
		    });
			
			return rackChildren;
			
		}
		$rootScope.findModulechildren = function(countryCode, storeNbr, rackIndex, modIndex) {
			var modChildren = [];
			$.each($rootScope.responseData, function(index, value) {
				if (value.countryCode === countryCode && value.storeNbr === storeNbr && value.rackIndex === rackIndex && value.modIndex === modIndex) {
					var sensorType = _.find(modChildren, {
						"FieldName" : value.sensorReadingType + "-" + value.sensorIOType
					})
					if (!sensorType) {
						var sensorType = {
							"FieldName" : value.sensorReadingType + "-" + value.sensorIOType,
							// "expanded": (typeof sensorQueryArray[4] !==
							// 'undefined' && sensorQueryArray[4]!==''),
							'level' : 5,
							"children" : []
						}
						modChildren.push(sensorType);
					}
					var sensor_Label = _.find(sensorType.children, {
						"FieldName" : value.sensorIndex + "-" + value.sensorlabel
					})

					if (!sensor_Label) {
						var sensor_Label = {
							"FieldName" : value.sensorIndex + "-" + value.sensorlabel,
							"fileicon" : true,
							"value" : value,
							'level' : 6,
						// "isSelected": (value.sensorIndex.toString() ===
						// sensorQueryArray[5])
						}
						sensorType.children.push(sensor_Label);
					}
				}
			});
			return modChildren;
		}

		$scope.search = function() {
			/* alert("Inside Search"); */
			$rootScope.loading = true;
			var params = $scope.sensorQuery.split('/');
			$scope.storeNumber=params[0];
			/*
			 * if ($scope.sensorQuery=='' && params.length < 2){ alert("Please
			 * Enter Country Code and Store number"); } else
			 */{

				var request = {
					"CC" : "US",
					"STNO" : params[0],
					"RID" : (params[1] ? params[1] : -1),
					"NETTYPE" : (params[2] ? params[2] : null),
					"MID" : (params[3] ? params[3] : -1),
					"IOTYPE" : (params[4] ? params[4] : null),
					"SENSID" : (params[5] ? params[5] : -1),

					"SENREADTYPE" : null

				}

				$$SensorTrendStoreServ.sensorTrend(request).then(success, fail);

				function success(response) {
					// console.log(angular.toJson(response));
					var headerData = {
						storeNumber : response.data.businessUnitDetail[0].businessUnit.number,
						storeDescription : response.data.businessUnitDetail[0].businessUnit.banner.description,
						managerName : response.data.businessUnitDetail[0].businessUnit.manager.name.fullName,
						storeLocation : response.data.businessUnitDetail[0].businessUnit.name,
						address : response.data.businessUnitDetail[0].facilityDetails[0].location.locationAddress[0].addressLine1,
						managerContactDetails : response.data.businessUnitDetail[0].businessUnit.contact[0].phone[0].number,
						dept1name : response.data.businessUnitDetail[0].divisionDepartments
					/* dept1number:response.data.businessUnitDetail[0].divisionDepartments[13].contact[0].phone[0].number */
					}
					$rootScope.headerData = headerData;
					$rootScope.loading = false;

				}

				function fail(error) {
					/*
					 * if(error.data.errorCode=='400') alert("Please enter valid
					 * store Number"); console.log("Please enter in given
					 * format");
					 * 
					 * console.log("Error");
					 */
					$rootScope.loading = false;
				}

			}

			$$SensorTrendSensorList.sensorTrend(request).then(successList, failList);

			function successList(response) {
				// console.log(response);
				var onPageLoadMsg = {
					"sensorStatus" : "",
					"sensorLbl" : ""
				}
				if (response.data[0].responseMessage == "SUCCESS" && response.data.length == 1) {
					// onPageLoadMsg.sensorStatus = 1;
					// onPageLoadMsg.sensorLbl =response.data[0].sensorlabel;
					// $rootScope.lastSelectedStore.CC = "US";
					// $rootScope.lastSelectedStore.Langcd = 0
					// $rootScope.lastSelectedStore.STNO =
					// response.data[0].storeNbr
					// $rootScope.lastSelectedStore.RID
					// =response.data[0].rackIndex
					// $rootScope.lastSelectedStore.NETTYPE="EP2"
					// $rootScope.lastSelectedStore.MID=response.data[0].modIndex
					// $rootScope.lastSelectedStore.IOTYPE=response.data[0].sensorIOType
					// $rootScope.lastSelectedStore.SENSID=response.data[0].sensorIndex
					// $rootScope.lastSelectedStore.SENREADTYPE=
					// response.data[0].sensorReadingType
					// $rootScope.lastSelectedStore.TI= -4
					// $rootScope.$broadcast("onPageLoad" , onPageLoadMsg);
					$scope.getData(response.data[0], null);

					// $$SharedDataService.setSenSelResp(successResp);
					// $$SharedDataService.setResponseData(successResp);
					// $rootScope.$broadcast("SensorSelected" ,
					// mSensorSelectedData);
					// $rootScope.loading = false;

				} else if (response.data[0].responseMessage == "SUCCESS" && response.data.length > 1) {
					onPageLoadMsg.sensorStatus = 2;
					$rootScope.$broadcast("onPageLoad", onPageLoadMsg);
				} else if (response.data[0].responseMessage == "No records found" || response.data[0].responseMessage == "FAILURE") {
					onPageLoadMsg.sensorStatus = -1;
					$rootScope.$broadcast("onPageLoad", onPageLoadMsg);
					$$Helper.printConsole("No records found")
				}
				$scope.resMessage=response.data[0].responseMessage;
				$scope.showSearchMessage = false;
				$rootScope.responseData = response.data;
				$rootScope.roleList = mapper(response.data, $scope.sensorQuery, true);
				$scope.myData = mapper(response.data, $scope.sensorQuery, false);
				$rootScope.loading = false;
			}

			function failList(error) {
				
				$rootScope.loading = false;
			}
		}

		$scope.filter = function() {
			if ($scope.treeSearchQuery) {
				var input = angular.copy($scope.roleList);
				$scope.myData = findNode(input, $scope.treeSearchQuery);
			} else {
				$scope.myData = mapper($rootScope.responseData, $scope.sensorQuery, false);
			}
			if($scope.myData.length ===0){
				$scope.showSearchMessage = true;	
			}
			else{
				$scope.showSearchMessage = false;
			}
		}
		function findNode(input, nodeName) {
			var output = [];
			_.each(input, function(obj) {
				if (obj.FieldName.match(new RegExp(nodeName, "i"))) {
					output.push(obj);
				} else if (obj.children) {
					var result = findNode(obj.children, nodeName);
					if (result && result.length > 0) {
						obj.children = result;
						obj.expanded = true;
						output.push(obj);
					}
				} else {
					return null;
				}
			});
			return output;
		}
		var oldNode;
		$scope.getData = function(value, node) {
			mSensorSelectedData = {
				sensorDetailsArgs : {
					"CC" : "US",
					"Langcd" : 100 || 0,
					"STNO" : value.storeNbr,
					"RID" : value.rackIndex,
					"MID" : value.modIndex,
					"IOTYPE" : value.sensorIOType,
					"SENSID" : value.sensorIndex,
					"NETTYPE" : "EP2",
					"SENREADTYPE" : value.sensorReadingType,
					"TI" : -4
				},

				"sensorLabel" : value.sensorlabel,
				"sensorStatus" : 1
			}
			$rootScope.lastSelectedStore = mSensorSelectedData.sensorDetailsArgs;

			if (oldNode && oldNode !== node) {
				oldNode.isSelected = false;
			}
			oldNode = node;
			/*if(!node.isSelected){
				$('#myModal').modal('show');
			}*/
			$rootScope.loading = true;
			$$SensorTrendDetailsServ.sensorDetails(mSensorSelectedData.sensorDetailsArgs).then(detailServSuccess, detailsServError);

		}

		function detailServSuccess(successResp) {
			$$SharedDataService.setSenSelResp(successResp);
			$$SharedDataService.setResponseData(successResp);
			$state.go('sensorTrend.graphView');
			$rootScope.$broadcast("SensorSelected", mSensorSelectedData);
			$rootScope.loading = false;
		}

		function detailsServError(errorResp) {
			console.log("Error while interacting to back end");
		}
		$scope.search();
		// $rootScope.loading = false;
		$scope.Enterfunc = function(keyEvent) {
			if (keyEvent.which == 13)
				$scope.search();
		}

		if (!$rootScope.freezedTrend) {
			$interval.cancel(freezeInterval);
			if (angular.isDefined(freezeInterval)) {
				return;
			} else {
				freezeInterval = $interval(callTrends, $$Constant.FREEZEINTERVAL);
			}

		} else {
			$interval.cancel(freezeInterval);
		}

		function callTrends() {
			if (!$rootScope.freezedTrend)
				$$SensorTrendDetailsServ.sensorDetails($rootScope.lastSelectedStore).then(intServSuccess, intServError);
		}

		$scope.$on('$destroy', function() {
			$interval.cancel(freezeInterval);
		});

		function intServSuccess(successResp) {
			$rootScope.$broadcast("FreezeTrend", successResp);
		}

		function intServError(errorResp) {
			console.log("ERROR WHILE CONNECTING TO BACKEND");
		}

	})

})();

(function(angular) {
	'use strict';
	angular
			.module('angularTreeview', [])
			.directive(
					'treeModel',
					[
							'$compile',
							'$rootScope',
							function($compile, $rootScope) {
								return {
									restrict : 'A',
									link : function(scope, element, attrs) {
										/* var start = new Date().getTime(); */
										var treeId = attrs.treeId;
										var treeModel = attrs.treeModel;
										var nodeId = attrs.nodeId;
										var nodeLabel = attrs.nodeLabel;
										var nodeChildren = attrs.nodeChildren;
										var searchQuery = attrs.searchQuery;
										var template = '<ul>' + '<li data-ng-repeat="node in '
												+ treeModel
												+ ' | filter:'
												+ searchQuery
												+ ' ">'
												+ '<i class="collapsed glyphicon glyphicon-triangle-right" data-ng-class="{nopointer: !node.'
												+ nodeChildren
												+ '.length}"'
												+ 'data-ng-show="!node.expanded && !node.fileicon" data-ng-click="'
												+ treeId
												+ '.selectNodeHead(node)"></i>'
												+ '<i class="expanded glyphicon glyphicon-triangle-bottom" data-ng-show="node.expanded && !node.fileicon" data-ng-click="'
												+ treeId
												+ '.selectNodeHead(node)"></i>'
												+ '<input type="checkbox" style="cursor:pointer" data-ng-show="node.fileicon" ng-model="node.isSelected" ng-click="getData(node.value, node)"/> '
												+ '<span title="{{node.' + nodeLabel + '}}">{{node.' + nodeLabel + '}}</span>'
												+ '<div data-ng-show="node.expanded" data-tree-id="' + treeId + '" data-tree-model="node.' + nodeChildren
												+ '" data-node-id=' + nodeId + ' data-node-label=' + nodeLabel + ' data-node-children=' + nodeChildren
												+ ' data-search-query=' + searchQuery + '></div>' + '</li>' + '</ul>';
										if (treeId && treeModel) {
											if (attrs.angularTreeview) {
												scope[treeId] = scope[treeId] || {};
												scope[treeId].selectNodeHead = scope[treeId].selectNodeHead
														|| function(selectedNode) {
															if (selectedNode[nodeChildren] !== undefined && selectedNode[nodeChildren].length > 0) {
																selectedNode.expanded = !selectedNode.expanded;
															} else if (selectedNode.level === 3) {
																selectedNode.children = $rootScope.findRackchildren(selectedNode.countryCode,
																		selectedNode.storeNbr, selectedNode.rackIndex);
																selectedNode.expanded = !selectedNode.expanded;
															} else if (selectedNode.level === 4) {
																selectedNode.children = $rootScope.findModulechildren(selectedNode.countryCode,
																		selectedNode.storeNbr, selectedNode.rackIndex, selectedNode.modIndex);
																selectedNode.expanded = !selectedNode.expanded;
															}
														};
												scope[treeId].selectNodeLabel = scope[treeId].selectNodeLabel || function(selectedNode) {
													if (scope[treeId].currentNode && scope[treeId].currentNode.selected) {
														scope[treeId].currentNode.selected = undefined;
													}
													selectedNode.selected = 'selected';
													scope[treeId].currentNode = selectedNode;
												};
											}
											element.html('').append($compile(template)(scope));
											/*
											 * var end = new Date().getTime();
											 * console.log(end-start);
											 */
										}
									}
								};
							} ]);
})(angular);
