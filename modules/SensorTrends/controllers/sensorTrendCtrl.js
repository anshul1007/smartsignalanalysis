warpIOTApp.controller("sensorTrendCtrl", function($scope, $timeout, $$Constant, $interval, $$SensorTrendDetailsServ, $rootScope, $$SharedDataService,
		$$SensorTrendDetailsServ) {
//  Initializing SelectedTab in case a refresh is done on SensorTrend Page
	$rootScope.selectedTab = 2;
	$scope.showTreeView = false;
	$scope.graphView = 'Graph View';
	$scope.tableView = 'Table View';
	$scope.btmboderGraphClass = "btnBorder";
	$scope.btmboderInTempClass = "btnBorder";
	$scope.temperature = "60 ";
	$scope.wind = "0.5km/h";
	$scope.humidity = "30%";
	$scope.checkboxModel = {
		freeze : false
	};	
	$scope.sensorLabel = "";
	var freezeInterval;
	$scope.sensorSelection = {
		status : "",
		sensordetailsMessage : ""

	}
// Function to set previously selected time interval. If first load, by default 4hr should be used.
	$scope.setTimeInterval = function() 
	{
		switch ($rootScope.selectedTimeInterval) {
		case 1:
			$scope.active1hr = "active";
			break;
		case 2:
			$scope.active2hr = "active";
			break;
		case 4:
			$scope.active4hr = "active";
			break;
		case 8:
			$scope.active8hr = "active";
			break;
		case 24:
			$scope.active24hr = "active";
			break;
		case 48:
			$scope.active48hr = "active";
			break;
		case 72:
			$scope.active72hr = "active";
			break;
		default:
			$scope.active4hr = "active";
		}
	};
// Function call for first load for the controller.
	$scope.setTimeInterval();

	$timeout(function() {
		angular.element(document.querySelector('#graphViewId')).triggerHandler('click');
	}, 0);

	$scope.toggleTreeView = function() {
		$scope.showTreeView = !$scope.showTreeView;
		$timeout(function(){
			$rootScope.$broadcast("TimeIntervalSelected", $rootScope.lastSelectedStore);
		} , 500)
	}

	$scope.selectView = function(view) {
		if (view == 1) {
			$scope.btmboderTableClass = "";
			$scope.btmboderGraphClass = "btnBorder";
		} else {
			$scope.btmboderTableClass = "btnBorder";
			$scope.btmboderGraphClass = "";
		}
	};

	var weather1Flag = true;
	var weather2Flag = true;
	$scope.selectWeather = function(weather) {

		if (weather == 1 && weather1Flag) {
			$scope.isFlipped = !$scope.isFlipped;
			$scope.btmboderOutTempClass = "";
			$scope.btmboderInTempClass = "btnBorder";
			$scope.temperature = "60 ";
			$scope.wind = "0.5km/h";
			$scope.humidity = "30%";
			weather1Flag = false;
			weather2Flag = true;
		} else if (weather == 2 && weather2Flag) {
			weather1Flag = true;
			weather2Flag = false;
			$scope.isFlipped = !$scope.isFlipped;
			$scope.btmboderOutTempClass = "btnBorder";
			$scope.btmboderInTempClass = "";
			$scope.temperature = "98 ";
			$scope.wind = "1.6km/h";
			$scope.humidity = "80%";
		}
	};

	$scope.trend4hr = function(timeInterval) {
		$rootScope.loading = true;
		$rootScope.lastSelectedStore.TI = "-" + timeInterval;
		$$SensorTrendDetailsServ.sensorDetails($rootScope.lastSelectedStore).then(detailServSuccess, detailsServError);
		resetTimeInterval();
		switch (timeInterval) {
		case 1:
			$scope.active1hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 2:
			$scope.active2hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 4:
			$scope.active4hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 8:
			$scope.active8hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 24:
			$scope.active24hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 48:
			$scope.active48hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		case 72:
			$scope.active72hr = "active";
			$rootScope.selectedTimeInterval = timeInterval;
			break;
		default:
			$scope.active4hr = "active";
		}

	}

	function detailServSuccess(successResp) {
		$$SharedDataService.resetSharedVariables();
		$$SharedDataService.setTimeIntervalResonse(successResp);
		$rootScope.$broadcast("TimeIntervalSelected", $rootScope.lastSelectedStore);
		$rootScope.loading = false;
	}

	function detailsServError(errorResp) {
		console.log("Error while interacting to back end");
	}

	

	$scope.freeze = function() {
		if ($scope.checkboxModel.freeze) {
			$rootScope.freezedTrend = true;
		} else {
			$rootScope.freezedTrend = false;
		}

	}

	
	$scope.$on('SensorSelected', function(event, args) {
		resetTimeInterval();
		$scope.setTimeInterval();
		$scope.sensorSelection.status = args.sensorStatus;
		$scope.sensorLabel = args.sensorLabel;
	})

	$scope.$on('onPageLoad', function(event, args) {
		$scope.sensorLabel = args.sensorLbl;
		$scope.sensorSelection.isSensorSel = true
		if (args.sensorStatus == -1) {
			// no record found
			$scope.sensorSelection.status = args.sensorStatus;
			$scope.sensorSelection.sensordetailsMessage = "Invalid Input or Please choose some other time interval"
			// console.log(" Trend " + "No record Found");
		} else if (args.sensorStatus == 1) {
			// single record found from top search of sensor tree
			$scope.sensorSelection.status = args.sensorStatus;
			// $rootScope.$broadcast("onPageLoad1", 1);
			resetTimeInterval();
			$scope.setTimeInterval();
		} else if (args.sensorStatus == 2) {
			// multiple record found from top sensor search
			$scope.sensorSelection.status = args.sensorStatus;
			$scope.sensorSelection.sensordetailsMessage = "Please choose a sensor from the list !"
		}
		// console.log(" ON Page Load");
	})

	function resetTimeInterval() {
		$scope.active1hr = "";
		$scope.active2hr = "";
		$scope.active4hr = "";
		$scope.active8hr = "";
		$scope.active24hr = "";
		$scope.active48hr = "";
		$scope.active72hr = "";
	}

});