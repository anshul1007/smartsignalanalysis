warpIOTApp.controller("SensorTrendsGraphCtrl", function($scope, $$SensorTrendDetailsServ, $$Helper, $interval, $rootScope, $timeout, $$Constant,
		$$SharedDataService) {

	$scope.freezeInterval = "";
	var reading = "";
	var timestamp = "";
	var trendSeries = [];
	// $scope.tableViewData = [];
	var threshLow = [];
	var threshHigh = [];
	$scope.value = ""
	$scope.graphType = "analog";
	$scope.yHigh = "ON";
	$scope.yLow = "OFF";
	
	$scope.detailsValue = {
		cutIn : "",
		cutOut : "",
		odometerValue : "",
		threshllastSelectedStoreow : "",
		threshhigh : "",
		lastupdate : "",
		sensorReadingTypeValue : "",
		sensorDTValue : "",
		deforstDuration : "",
		lastDeforstTime : "",
		bShowRhs : false,
		isStoreNbrAvble : ""
	}
	var lTableViewData = [];
	

	$scope.$on('SensorSelected', function(event, args) {
		var lSenSelRespData = $$SharedDataService.getSenSelResp();
		if (lSenSelRespData.length == 0) {
			// console.log(" Sensor Details Args " +
			// angular.toJson(args.sensorDetailsArgs));
			$$Helper.printConsole(" Sensor Details Args " + angular.toJson(args.sensorDetailsArgs));
			$rootScope.loading = true;
			$$SensorTrendDetailsServ.sensorDetails(args.sensorDetailsArgs).then(detailServSuccess, detailsServError);
			$rootScope.lastSelectedStore = args.sensorDetailsArgs;
		} else if (lSenSelRespData.length != 0) {
			detailServSuccess(lSenSelRespData);
		}
		
		$timeout(function() {
			angular.element(document.querySelector('#graphViewId')).triggerHandler('click');
		}, 0);
	})
	
	$scope.$on('TimeIntervalSelected', function(event, args) {
		// console.log("Inside Time Selected");
		var lTimeIntSel = $$SharedDataService.getTimeIntervalResonse();
		if (lTimeIntSel.length == 0) {
			// console.log("Inside Time Selected LResp is null");
			$$Helper.printConsole("TimeInterval " + angular.toJson(args));
			$rootScope.loading = true;
			$$SensorTrendDetailsServ.sensorDetails(args).then(detailServSuccess, detailsServError);
			$rootScope.lastSelectedStore = args;
		} else if (lTimeIntSel.length != 0) {
			// console.log("Inside Time Selected LResp is not null");
			// console.log("not null " + angular.toJson(lTimeIntSel));
			detailServSuccess(lTimeIntSel);
		}
	})

	$scope.$on('FreezeTrend', function(event, args) {
		if (!$rootScope.freezedTrend) {
			detailServSuccess(args);
		}
	})

	

	if ($rootScope.lastSelectedStore.length != 0) {
		var lResponseData = $$SharedDataService.getResponseData();
		// console.log("Default Object Length" + lResponseData.data.length)
		if (lResponseData.data.length == 0) {
			$$SensorTrendDetailsServ.sensorDetails($rootScope.lastSelectedStore).then(detailServSuccess, detailsServError);
		} else if (lResponseData.data.length != 0) {
			detailServSuccess($$SharedDataService.getResponseData());
		}
	}

	function detailServSuccess(successResponse) {
		// console.log("Success Resp " + angular.toJson(successResponse));
		$$SharedDataService.setResponseData(successResponse);
		$rootScope.loading = false;
		// console.log("StoreNrb " +
		// successResponse.data[0].storeNbr);
		$scope.detailsValue.isStoreNbrAvble = successResponse.data[0].storeNbr;
		if (successResponse.data[0].storeNbr == "null") {
			$rootScope.loading = false;
		}
		if (successResponse.data[0].sensorReadingType == "ANALOG") {
			$scope.graphType = "analog"
			$scope.detailsValue.bShowRhs = true;
			sensorReadingRhs(successResponse);
			analogGraph(successResponse);
		} else if (successResponse.data[0].sensorReadingType == "DIGITAL") {
			$scope.graphType = "digital"
			$scope.detailsValue.bShowRhs = true;
			digitalGraph(successResponse);
			sensorReadingRhs(successResponse);
		} else {
			// remove RHS when it is not neither ANALOG nor
			// DIGITAL
			$scope.detailsValue.bShowRhs = false;
		}

	}
	function sensorReadingRhs(successResponse) {
		// resetVariable();
		$scope.detailsValue.sensorReadingTypeValue = successResponse.data[0].sensorReadingType;
		$scope.detailsValue.odometerValue = successResponse.data[0].reading;
		$scope.detailsValue.cutIn = successResponse.data[0].cutInValue;
		$scope.detailsValue.cutOut = successResponse.data[0].cutOutValue;
		for (var index = successResponse.data.length; index > 0; index--) {
			$scope.detailsValue.threshlow = calcThreshold(successResponse, index - 1, 1);
			$scope.detailsValue.threshhigh = calcThreshold(successResponse, index - 1, 2);
		}
	
		$scope.detailsValue.lastupdate = $$Helper.timeStampConversion(successResponse.data[0].eventTimestamp);
		$scope.detailsValue.deforstDuration = successResponse.data[0].deforstDuration;
		$scope.detailsValue.lastDeforstTime = successResponse.data[0].lastDeforstTime;
		if (successResponse.data[0].sensorReadingType == "DIGITAL") {
			$scope.detailsValue.threshlow = successResponse.data[0].val1;
		}
		if (successResponse.data[0].sensorReadingType == "ANALOG") {
			var otherValues = [ $scope.detailsValue.odometerValue, $scope.detailsValue.threshlow, $scope.detailsValue.threshhigh, $scope.detailsValue.cutIn,
					$scope.detailsValue.cutOut ];
			$scope.lowerLimit = parseInt(Math.round(getMinLimit(otherValues))) - 10;
			$scope.upperLimit = parseInt(Math.round(getMaxLimit(otherValues))) + 10;
			// console.log( " upperLimit = " + $scope.upperLimit + " lowerLimit
			// = " + $scope.lowerLimit );
			$scope.value = $scope.detailsValue.odometerValue;
			$scope.unit = "";
			$scope.precision = 2;
			var lowCutIn = getMinLimit([ $scope.detailsValue.cutIn, $scope.detailsValue.cutOut ]);
			var lowCutOut = getMinLimit([ $scope.detailsValue.threshlow, $scope.detailsValue.threshhigh ]);
			var highCutIn = getMaxLimit([ $scope.detailsValue.cutIn, $scope.detailsValue.cutOut ]);
			var highCutOut = getMaxLimit([ $scope.detailsValue.threshlow, $scope.detailsValue.threshhigh ]);
			if ($scope.detailsValue.cutIn == '' && $scope.detailsValue.cutOut == '' && $scope.detailsValue.cutIn != 0 && $scope.detailsValue.cutOut != 0) {
				// no cutin cutout
				$scope.ranges = [ {
					min : $scope.lowerLimit,
					max : $scope.upperLimit,
					color : '#ff0100' // red
				}, {
					min : $scope.detailsValue.threshlow,
					max : $scope.detailsValue.threshhigh,
					color : '#90D050' // green
				} ];
			} else {
				$scope.ranges = [ {
					min : $scope.lowerLimit,
					max : $scope.upperLimit,
					color : '#ff0100' // red
				}, {
					min : lowCutIn,
					max : lowCutOut,
					color : '#FEC21E' // yellow
				}, {
					min : highCutIn,
					max : highCutOut,
					color : '#FEC21E' // yellow
				}, {
					min : $scope.detailsValue.cutIn,
					max : $scope.detailsValue.cutOut,
					color : '#90D050' // green
				} ];
			}
			/*
			 * console.log( "value = " + $scope.detailsValue.odometerValue + "
			 * cutIn = " + $scope.detailsValue.cutIn + " cutOut = " +
			 * $scope.detailsValue.cutOut + " threshlow = " +
			 * $scope.detailsValue.threshlow + " threshhigh = " +
			 * $scope.detailsValue.threshhigh + " lowCutIn = " + lowCutIn + "
			 * lowCutOut = " + lowCutOut + " highCutIn = " + highCutIn + "
			 * highCutOut = " + highCutOut + " sensorReadingTypeValue = " +
			 * $scope.detailsValue.sensorReadingTypeValue + " deforstDuration = " +
			 * $scope.detailsValue.deforstDuration + " lastDeforstTime = " +
			 * $scope.detailsValue.lastDeforstTime );
			 */

			// }
			/*
			 * function updateNeedle() { $timeout(function() {
			 * $scope.value=$scope.value + 0.1;
			 * 
			 * if ($scope.value > $scope.upperLimit) {
			 * $scope.value=$scope.lowerLimit; } //update(); }, 1000); }
			 * updateNeedle();
			 */

		}

	}
	function analogGraph(successResponse) {
		resetVariable();

		$$Helper.printConsole("Success CTRL Response " + successResponse.data.length);
		for (var index = successResponse.data.length; index > 0; index--) {
			// console.log(" Index 1" + new
			// Date(successResponse.data[index-1].eventTimestamp).getTime()/1000);

			trendSeries.push({
				x : new Date($$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp)).getTime() / 1000,
				y : parseFloat(successResponse.data[index - 1].reading)
			})

			threshLow.push({
				x : new Date($$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp)).getTime() / 1000,
				y : calcThreshold(successResponse, index - 1, 1)
			})

			threshHigh.push({
				x : new Date($$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp)).getTime() / 1000,
				y : calcThreshold(successResponse, index - 1, 2)
			})

			// $scope.tableViewData = successResponse.data;

			lTableViewData.push({
				timestamp : $$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp),
				reading : successResponse.data[index - 1].reading,
				lowerLimit : successResponse.data[index - 1].val1,
				upperLimit : successResponse.data[index - 1].val2,
				defrost : successResponse.data[index - 1].deforstDuration
			})
		}

		$scope.sightingsByDate = [ {
			data : trendSeries,
			color : '#78b9e7',
			renderer :'lineplot'
		}, {
			data : threshLow,
			color : 'blue',
			renderer :'line'
		}, {
			data : threshHigh,
			color : 'green',
			renderer :'line'
		} ];
		$scope.tableViewData = lTableViewData;
		$$Helper.printConsole("Index 2 " + trendSeries.length);
		// $scope.sightingsByDate = trendSeries;
		$rootScope.loading = false;

	}

	function digitalGraph(successResponse) {
		resetVariable();
		// $scope.value = 0;

		for (var index = successResponse.data.length; index > 0; index--) {

			trendSeries.push({
				x : new Date($$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp)).getTime() / 1000,
				y : digiReadingConversion(successResponse.data[index - 1].reading)
			})

			lTableViewData.push({
				timestamp : $$Helper.timeStampConversion(successResponse.data[index - 1].eventTimestamp),
				reading : successResponse.data[index - 1].reading,
				lowerLimit : successResponse.data[index - 1].val1
			// upperLimit : successResponse.data[index - 1].val2,
			// defrost : successResponse.data[index -
			// 1].deforstDuration
			})
		}

		// console.log("Digital Reading " +
		// angular.toJson(trendSeries));
		$scope.tableViewData = lTableViewData;
		$scope.sightingsByDate = [ {
			data : trendSeries,
			color : '#78b9e7',
			renderer :'lineplot'
		} ];
		$rootScope.loading = false;
	}
	// console.log(" Changing View " +trendSeries);
	function detailsServError(errorResponse) {
		console.log("Error CTRL Response" + errorResponse);
		$rootScope.loading = false;
		$scope.detailsValue.isStoreNbrAvble = null;

	}

	$scope.checkboxModel = {
		threshold : true
	// defrost : true
	};
	$scope.tableViewData = lTableViewData;
	function resetVariable() {
		trendSeries = [];
		threshLow = [];
		threshHigh = [];
		lTableViewData = []
	}

	function digiReadingConversion(digiReading) {
		if (digiReading == "OPEN" || digiReading == "ON" || digiReading == "ACTIVE") {
			$scope.yHigh = digiReading; 
			return 1;
		}

		if (digiReading == "CLOSED" || digiReading == "OFF" || digiReading == "INACTIVE") {
			$scope.yLow = digiReading;
			// console.log("CLOSED")
			return 0;
		}

	}

	$scope.showGraph = function() {
		if (!$scope.checkboxModel.threshold) {
			$scope.sightingsByDate = [ {
				data : trendSeries,
				color : '#78b9e7',
				renderer :'lineplot'
			} ]
		} else {
			$scope.sightingsByDate = [ {
				data : trendSeries,
				color : '#78b9e7',
				renderer :'lineplot'
			}, {
				data : threshLow,
				color : 'blue',
				renderer :'line'
			}, {
				data : threshHigh,
				color : 'green',
				renderer :'line'
			} ]
		}
	}
	function getMinLimit(pData) {
		return Math.min.apply(Math, pData);
	}

	function getMaxLimit(pData) {
		return Math.max.apply(Math, pData);
	}
	function calcThreshold(pData, index, choseThresh) {
		if (index < 0)
			return 0;
		if (choseThresh == 1) {
			// low threshold
			if (pData.data[index].val1.length != 0) {
				return parseFloat(pData.data[index].val1);
			} else if (pData.data[index].val1.length == 0) {
				return calcThreshold(pData, index - 1, 1);
			}
		} else if (choseThresh == 2) {
			// high threshold
			if (pData.data[index].val2.length != 0) {
				return parseFloat(pData.data[index].val2);
			} else if (pData.data[index].val2.length == 0) {
				return calcThreshold(pData, index - 1, 2);

			}
		}
	}

});


