warpIOTApp.factory('$$SensorTrendDetailsServ' , function($q , $http , $$Helper ,$$Constant){

	var service = {}; 
	
	service.sensorDetails = function(pData) {
		
		var lSensorDetails = $$Helper.selectEnvironement($$Constant.SELECTENV);
		$$Helper.printConsole("Sensor Details URL "
				+ lSensorDetails.URL.SENSORDETAILS);
		
		var sensorDetails = $q.defer();
		
		$http({
			method : "POST",
			url : lSensorDetails.URL.SENSORDETAILS,
			data: pData,
			
			headers : {'Content-Type': 'application/json; charset=utf-8'}
			
			
		}).then(function(response) {
			$$Helper.printConsole("Success Response  " + angular.toJson(response) );
			sensorDetails.resolve(response);
			
		}, function (response) {
			$$Helper.printConsole("Success Error  " + angular.toJson(response) );
			sensorDetails.reject(response);
			
		});
		return sensorDetails.promise;
	};
	
	return service;

});
