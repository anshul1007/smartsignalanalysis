warpIOTApp.factory('$$SensorTrendStoreServ', function($q, $http, $$Helper,
		$$Constant) {

	var service = {};

	service.sensorTrend = function(pData) {
		var lSensorInfo = $$Helper.selectEnvironement($$Constant.SELECTENV);
		$$Helper.printConsole("Store Info " + lSensorInfo.URL.STOREINFO);

		var url = $$Helper.format(lSensorInfo.URL.STOREINFO, pData.CC, "100",
				pData.STNO);
		var sensorTrend = $q.defer();

		$http({
			method : "GET",
			url : url,
			// data: pData,
			headers : 'Content-Type: application/json'

		}).then(function(response) {
			// console.log("Success Response " + angular.toJson(response));
			sensorTrend.resolve(response);
		}, function(response) {
			// console.log("Success Error " + angular.toJson(response));
			sensorTrend.reject(response);
		});
		return sensorTrend.promise;
	};

	return service;

});
