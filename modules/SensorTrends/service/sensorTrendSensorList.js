warpIOTApp.factory('$$SensorTrendSensorList' , function($q , $http , $$Helper ,$$Constant){

	var service = {}; 
	
	service.sensorTrend = function(pData) {
		
		var lSensorList = $$Helper.selectEnvironement($$Constant.SELECTENV);
		$$Helper.printConsole("Sensor List " + lSensorList.URL.SENSORLIST);
		var sensorTrend = $q.defer();
		
		$http({
			method : "POST",
			url : lSensorList.URL.SENSORLIST,
			data: pData,
			headers : 'Access-Control-Allow-Origin'
			
		}).then(function(response) {
			//console.log("Success Response  " + angular.toJson(response));
			sensorTrend.resolve(response);
		}, function (response) {
			//console.log("Success Error  " + angular.toJson(response));
			sensorTrend.reject(response);
		});
		return sensorTrend.promise;
	};
	

	
	return service;

});
