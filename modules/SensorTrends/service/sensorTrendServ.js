warpIOTApp.factory('$$SensorTrendServ' , function($q , $http , $$Helper ,$$Constant){

	var service = {}; 
	
	service.sensorTrend = function(pData) {
		
		var lSensorInfo = $$Helper.selectEnvironement($$Constant.SELECTENV);
		$$Helper.printConsole("Store Info " + lSensorInfo.URL.STOREINFO);
		var sensorTrend = $q.defer();
		
		$http({
			method : "GET",
			url : lSensorInfo.URL.STOREINFO,
			data: pData,
			headers : 'Content-Type: application/json'
			
		}).then(function(response) {
			console.log("Success Response  " + angular.toJson(response));
			sensorTrend.resolve(response);
		}, function (response) {
			console.log("Success Error  " + angular.toJson(response));
			sensorTrend.reject(response);
		});
		return sensorTrend.promise;
	};
	
	return service;

});
