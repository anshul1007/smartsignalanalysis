var gulp = require('gulp');
var clean = require('gulp-clean');
var war = require('gulp-war');
var zip = require('gulp-zip');

gulp.task('MoveJSLib' , function(){
	gulp.src(['node_modules/angular/angular.min.js',
	          'node_modules/angular-chart.js/dist/angular-chart.min.js',
	          'node_modules/angular-ui-router/release/angular-ui-router.min.js',
	          'node_modules/bootstrap/dist/js/bootstrap.min.js',
	          'node_modules/chart.js/dist/Chart.min.js'
	        ])
		.pipe(gulp.dest('lib/js/'))
});

gulp.task('MoveCSSLib' , function(){
	gulp.src(['node_modules/bootstrap/dist/css/bootstrap.min.css'
	        ])
		.pipe(gulp.dest('lib/css/'))
});

gulp.task('MoveFONTLib' , function(){
	gulp.src(['node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.eot',
	          'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.svg',
	          'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.ttf',
	          'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff',
	          'node_modules/bootstrap/dist/fonts/glyphicons-halflings-regular.woff2'
	        ])
		.pipe(gulp.dest('lib/fonts/'))
});

gulp.task('CleanJSLib', function () {
    return gulp.src('lib/js/', {read: false})
        .pipe(clean());
});

gulp.task('CleanCSSLib', function () {
    return gulp.src('lib/css/', {read: false})
        .pipe(clean());
});

gulp.task('CleanFONTLib', function () {
    return gulp.src('lib/fonts/', {read: false})
        .pipe(clean());
});

gulp.task('war', function () {
    gulp.src(['**'])
        .pipe(war({
            welcome: 'index.html',
            displayName: 'REIoT WAR',
        }))
        .pipe(zip('reiot-ui.war'))
        .pipe(gulp.dest("./dist"));

});
