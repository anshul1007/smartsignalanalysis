warpIOTApp
		.constant(
				"$$Constant",
				{
					"local_test_url" : {
						"STOREINFO" : "https://api.qa.wal-mart.com/si/telocmdm/locationservices/location/{0}/{1}/businessunits/{2}?q=details&client_id=9de10b4c-b2ea-4e82-81bf-bf7560788dbf",
						"SENSORDETAILS" : "http://services.test.refrigerationstorealert.borsfm.dev.cloud.wal-mart.com:8080/reiot-webui-services/getreiot/SensorDetails",
						"SENSORLIST" : "http://services.test.refrigerationstorealert.borsfm.dev.cloud.wal-mart.com:8080/reiot-webui-services/getreiot/SensorList"
					},
					
					"DEV_URL" : {
						"STOREINFO" : "https://api.qa.wal-mart.com/si/telocmdm/locationservices/location/{0}/{1}/businessunits/{2}?q=details&client_id=9de10b4c-b2ea-4e82-81bf-bf7560788dbf",
						"SENSORDETAILS" : "http://coredataservices.dev.reiot.borsfm.prod.cloud.wal-mart.com/reiot-webui-services/getreiot/SensorDetails",
						"SENSORLIST" : "http://coredataservices.dev.reiot.borsfm.prod.cloud.wal-mart.com/reiot-webui-services/getreiot/SensorList"
					},
					"STAGE_URL" : {
						"STOREINFO" : "https://api.qa.wal-mart.com/si/telocmdm/locationservices/location/{0}/{1}/businessunits/{2}?q=details&client_id=9de10b4c-b2ea-4e82-81bf-bf7560788dbf",
						"SENSORDETAILS" : "http://coredataservices.stage.reiot.borsfm.prod.cloud.wal-mart.com/reiot-webui-services/getreiot/SensorDetails",
						"SENSORLIST" : "http://coredataservices.stage.reiot.borsfm.prod.cloud.wal-mart.com/reiot-webui-services/getreiot/SensorList"
					},
					"PROD_URL" : {
						"STOREINFO": "TreeResponse_1029.txt",
				        "SENSORDETAILS": "http://coredataservices.prod.reiot.borsfm.prod.cloud.wal-mart.com/reiot-webui-services/getreiot/SensorDetails",
				        "SENSORLIST": "TreeJson-8-28.txt"
					},
					"consolePrint" : false,
					"FREEZEINTERVAL" : 300000,
					"SELECTENV":"PROD" //DEV , PROD , STAGE
				});