warpIOTApp.factory('$$Helper', function($$Constant) {

	var service = {};

	service.printConsole = function(msg) {
		if ($$Constant.consolePrint) {
			console.log(msg);
		}
	};

	service.format = function(format) {
		var args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};

	service.selectEnvironement = function(env) {
		if (env == "DEV") {
			return {
				URL : {
					"STOREINFO" : $$Constant.DEV_URL.STOREINFO,
					"SENSORDETAILS" : $$Constant.DEV_URL.SENSORDETAILS,
					"SENSORLIST" : $$Constant.DEV_URL.SENSORLIST
				}
			}
		} else if (env == "STAGE") {
			return {
				URL : {
					"STOREINFO" : $$Constant.STAGE_URL.STOREINFO,
					"SENSORDETAILS" : $$Constant.STAGE_URL.SENSORDETAILS,
					"SENSORLIST" : $$Constant.STAGE_URL.SENSORLIST
				}
			}

		} else if (env == "PROD") {
			return {
				URL : {
					"STOREINFO" : $$Constant.PROD_URL.STOREINFO,
					"SENSORDETAILS" : $$Constant.PROD_URL.SENSORDETAILS,
					"SENSORLIST" : $$Constant.PROD_URL.SENSORLIST
				}
			}
		}
	}

	service.timeStampConversion = function(date) {
		//console.log("UTC Time " + date);
		var lUTCFormat = moment.utc(date);
		lUTCFormat.tz('America/Chicago');
		
		var lCST = lUTCFormat.format('YYYY-MM-DD HH:mm:ss');
		//console.log("CST Time " + new Date(lCST).getTime()/1000);
		return lCST;
	}
	return service;

});
