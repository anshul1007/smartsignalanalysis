warpIOTApp.config(function($stateProvider, $urlRouterProvider) {

	$urlRouterProvider.otherwise('/taskList');

	$stateProvider

	.state('sensorTrend', {
		url : '/sensorTrend',
		
		views :{
			"":{
				templateUrl : 'modules/SensorTrends/pages/sensorTrendPage.html',
				controller:'sensorTrendCtrl'
			},
			'sidebar@sensorTrend' : {
				 templateUrl: 'modules/SensorTrends/pages/trendSidebar.html',
				 controller:'SensorTreeCtrl'
			},
			'storeNweatherinformation@sensorTrend' :{
				 templateUrl: 'modules/SensorTrends/pages/storeNweatherinfo.html',
				 controller:'sensorTrendCtrl'
			},
			'sensorSpecReadingInfo@sensorTrend' :{
				templateUrl: 'modules/SensorTrends/pages/sensorSpecReadingInfo.html',
				controller:'SensorTrendsGraphCtrl'
			}
		}
		
	})

	.state('sensorTrend.graphView', {
		url : '/graphView',
		templateUrl : 'modules/SensorTrends/pages/graphView.html',
		controller :'SensorTrendsGraphCtrl'
	})
	
	.state('sensorTrend.tableView', {
		url : '/tableView',
		templateUrl : 'modules/SensorTrends/pages/tableView.html',
		controller :'SensorTrendsGraphCtrl'
	})

	.state('Tasks', {
		// actual task list page will come here
		url : '/taskList',
		templateUrl : 'modules/TaskList/pages/Tasks.html'
	})
	.state('Tasks.Details', {
		url : null,
		views : {
				'taskadditionaldetails' : {
				templateUrl : 'modules/TaskList/pages/TasksDetails.html'
				},
				'taskeventdetails' : {
				templateUrl : 'modules/TaskList/pages/EventDetails.html'
				}
				},
				})
	.state('Tasks.Details.WORT', {
		url : null,
		sticky : true,
		views : {
				'RelTasksDiv' : {
				templateUrl : 'modules/TaskList/pages/RelTasks.html'
				}
				},
				})
	.state('Tasks.Details.Events', {
		url : null,
		sticky : true,
		views : {
				'QEventDiv' : {
				templateUrl : 'modules/TaskList/pages/QEventDetails.html'
				}
				},
				});

});

