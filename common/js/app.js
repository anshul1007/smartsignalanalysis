var warpIOTApp = angular.module('WARPIOTAPP', [ "angularTreeview",
		"ui.router",'ct.ui.router.extras.core', 'ct.ui.router.extras.sticky',
		'ngTable', 'ngSanitize', 'ngCsv', 'ngRadialGauge','ui.bootstrap','ngAnimate']);

		// register a new service
warpIOTApp.value('WARPIOTAPP', 'ngRadialGauge');

warpIOTApp.run(function($rootScope) {
    
	$rootScope.loading = false;
    
    $rootScope.odometerValue = "";
    
    $rootScope.freezedTrend = false;
// Initializing SelectedTab on initial load
	$rootScope.selectedTab = 1;
// To store last selected interval. Default is 4hrs
	$rootScope.selectedTimeInterval;
    
    $rootScope.lastSelectedStore = {
			"CC":"US",
			"Langcd":0 || 100,
			"STNO":"",
			"RID":"",
			"NETTYPE":"EP2",
			"MID":"",
			"IOTYPE":"",
			"SENSID":"",
			"SENREADTYPE":"",
			"TI":""
			};
})


