warpIOTApp.factory('$$SharedDataService', function() {

	var value = {
		"data" : "",
		"senselResp" : "",
		"timeIntResp" : ""
	};

	return {
		getResponseData : function() {
			return value.data;
		},
		setResponseData : function(pValue) {
			value.data = pValue;
		},
		getSenSelResp : function() {
			return value.senselResp;
		},
		setSenSelResp : function(senSelResponse) {
			value.senselResp = senSelResponse;
		},
		setTimeIntervalResonse : function(timeIntResponse) {
			value.timeIntResp = timeIntResponse;
		},
		getTimeIntervalResonse : function() {
			return value.timeIntResp;
		},
		resetSharedVariables : function() {
			var value = {
				"data" : "",
				"senselResp" : "",
				"timeIntResp" : ""
			};
		}
	};

});
